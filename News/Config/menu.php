<?php
return [
    'package' => 'solutions_news',
    'title' => ['ru' => 'Новости', 'en' => 'News', 'es' => 'Noticias'],
    'route' => 'solutions.news.index',
    'icon' => 'zmdi zmdi-radio',
    'menu_child' => []
];