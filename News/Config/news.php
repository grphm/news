<?php
return [
    'package_name' => 'solutions_news',
    'package_title' => ['ru' => 'Новости', 'en' => 'News', 'es' => 'Noticias'],
    'package_icon' => 'zmdi zmdi-radio',
    'relations' => ['core_content', 'core_galleries'],
    'package_description' => [
        'ru' => 'Позволяет управлять новостями. Используется модуль "Контент" и "Галереи"',
        'en' => 'It allows you to manage news. Using the module "Content" and "Galleries"',
        'es' => 'Se le permite administrar las noticias. Utilice el módulo de "Contenido" y "Galería"'
    ],
    'version' => [
        'ver' => 1.2,
        'date' => '23.01.2017'
    ]
];