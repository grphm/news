@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="active">
            <i class="{{ config('solutions_news::menu.icon') }}"></i> {!! array_translate(config('solutions_news::menu.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_news::menu.icon') }}"></i>
            {!! array_translate(config('solutions_news::menu.title')) !!}
        </h2>
    </div>
    @if(\PermissionsController::allowPermission('solutions_news', 'create', FALSE))
        @BtnAdd('solutions.news.create')
    @endif
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_news_lang::news.list')
                </div>
                @if($news->count())
                    {!! Form::open(['route' => 'solutions.news.index', 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search" placeholder="@lang('solutions_news_lang::news.search')" class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                    <ul class="actions">
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right z-index-max">
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_news_lang::news.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'published_start', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_news_lang::news.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'asc'])) }}">
                                        @lang('solutions_news_lang::news.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'title', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_news_lang::news.sort_title')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'published_start', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_news_lang::news.sort_published')
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('solutions.news.index', array_merge(Request::all(), ['sort_field' => 'updated_at', 'sort_direction' => 'desc'])) }}">
                                        @lang('solutions_news_lang::news.sort_updated')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($news as $news_single)
                    <div class="js-item-container list-group-item media">
                        @if($news_single->announce_image && \Storage::exists($news_single->announce_image))
                            <div class="pull-left clearfix">
                                <img alt="{{ $news_single->title }}" class="lgi-img"
                                     src="{{ asset('uploads/' . $news_single->announce_image) }}">
                            </div>
                        @endif
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    @if(\PermissionsController::allowPermission('solutions_news', 'edit', FALSE))
                                        <li>
                                            <a href="{{ route('solutions.news.edit', $news_single->id) }}">@lang('solutions_news_lang::news.edit')</a>
                                        </li>
                                    @endif
                                    @if($news_single->publication)
                                        <li>
                                            <a href="{{ route('public.news.show', $news_single->PageUrl) }}"
                                               target="_blank">
                                                @lang('solutions_news_lang::news.blank')
                                            </a>
                                        </li>
                                    @endif
                                    @if(\PermissionsController::allowPermission('solutions_news', 'delete', FALSE))
                                        <li class="divider"></li>
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_news_lang::news.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.news.destroy', $news_single->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_news_lang::news.delete.question') &laquo;{{ $news_single->title }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_news_lang::news.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_news_lang::news.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{{ $news_single->title }}</div>
                            <small class="c-grey f-10">
                                {{ $news_single->announce }}
                            </small>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_news_lang::news.empty')</h2>
                @endforelse
            </div>
            <div class="lg-pagination p-10">
                {!! $news->appends(\Request::only(['search', 'sort_field', 'sort_direction']))->render() !!}
            </div>

        </div>
    </div>
@stop