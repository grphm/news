<?php
return [
    'list' => 'Lista de noticias',
    'search' => 'Introduzca el título de la noticia',
    'sort_title' => 'Título',
    'sort_published' => 'Fecha de publicación',
    'sort_updated' => 'Fecha de actualización',
    'edit' => 'Editar',
    'blank' => 'Abrir en nueva ventana',
    'empty' => 'Lista está vacía',
    'delete' => [
        'question' => 'Eliminar noticias',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de noticias',
        'form' => [
            'title' => 'Título',
            'tags' => 'Etiquetas',
            'tags_help_description' => 'Lista separada por comas',
            'announce' => 'Anuncio',
            'content' => 'Contenido',
            'main_image' => 'Imagen principal',
            'main_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'announce_image' => 'Imagen del anuncio',
            'announce_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'gallery' => 'Galería',
            'publish' => 'Publisch',
            'top' => 'Top',
            'published_start' => 'Inicio publicaciones',
            'published_stop' => 'Fin de publicación',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar noticias',
        'form' => [
            'title' => 'Título',
            'tags' => 'Etiquetas',
            'tags_help_description' => 'Lista separada por comas',
            'announce' => 'Anuncio',
            'content' => 'Contenido',
            'main_image' => 'Imagen principal',
            'main_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'announce_image' => 'Imagen del anuncio',
            'announce_image_help_description' => 'Formatos soportados: png, jpg, gif',
            'image_select' => 'Selecto',
            'image_change' => 'Enmendar',
            'image_delete' => 'Eliminar',
            'gallery' => 'Galería',
            'publish' => 'Publisch',
            'top' => 'Top',
            'published_start' => 'Inicio publicaciones',
            'published_stop' => 'Fin de publicación',
            'submit' => 'Guardar'
        ]
    ]
];