<?php
return [
    'list' => 'Список новостей',
    'search' => 'Введите название новости',
    'sort_title' => 'Название',
    'sort_published' => 'Дата публикации',
    'sort_updated' => 'Дата обновления',
    'edit' => 'Редактировать',
    'blank' => 'Открыть в новом окне',
    'empty' => 'Список пустой',
    'delete' => [
        'question' => 'Удалить новость',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление новости',
        'form' => [
            'title' => 'Название',
            'tags' => 'Теги',
            'tags_help_description' => 'Перечислить через запятую',
            'announce' => 'Анонс',
            'content' => 'Содержание',
            'main_image' => 'Основное изображение',
            'main_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'announce_image' => 'Изображение анонса',
            'announce_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'gallery' => 'Галерея',
            'publish' => 'Опубликовать',
            'top' => 'Основная',
            'published_start' => 'Начало публикации',
            'published_stop' => 'Окончание публикации',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование новости',
        'form' => [
            'title' => 'Название',
            'tags' => 'Теги',
            'tags_help_description' => 'Перечислить через запятую',
            'announce' => 'Анонс',
            'content' => 'Содержание',
            'main_image' => 'Основное изображение',
            'main_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'announce_image' => 'Изображение анонса',
            'announce_image_help_description' => 'Поддерживаемые форматы: png, jpg, gif',
            'image_select' => 'Выбрать',
            'image_change' => 'Изменить',
            'image_delete' => 'Удалить',
            'gallery' => 'Галерея',
            'publish' => 'Опубликовать',
            'top' => 'Основная',
            'published_start' => 'Начало публикации',
            'published_stop' => 'Окончание публикации',
            'submit' => 'Сохранить'
        ]
    ]
];