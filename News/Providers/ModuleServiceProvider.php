<?php
namespace STALKER_CMS\Solutions\News\Providers;

use STALKER_CMS\Solutions\News\Http\Controllers\PublicNewsController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_news_views');
        $this->registerLocalization('solutions_news_lang');
        $this->registerConfig('solutions_news::config', 'Config/news.php');
        $this->registerSettings('solutions_news::settings', 'Config/settings.php');
        $this->registerActions('solutions_news::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_news::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
    }

    public function register() {

        \App::bind('PublicNewsController', function() {

            return new PublicNewsController();
        });
    }

    /********************************************************************************************************************/
    protected function registerBladeDirectives() {
    }

    public function publishesTemplates() {

        $this->publishes([
            __DIR__.'/../Resources/Templates' => base_path('home/Resources')
        ]);
    }
}
