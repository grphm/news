<?php
namespace STALKER_CMS\Solutions\News\Facades;

use Illuminate\Support\Facades\Facade;

class PublicNews extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicNewsController';
    }
}