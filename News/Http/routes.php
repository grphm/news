<?php
\Route::group(['prefix' => 'admin', 'middleware' => 'secure'], function() {

    \Route::resource('news', 'NewsController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.news.index',
                'create' => 'solutions.news.create',
                'store' => 'solutions.news.store',
                'edit' => 'solutions.news.edit',
                'update' => 'solutions.news.update',
                'destroy' => 'solutions.news.destroy'
            ]
        ]
    );
});
$news_segment = settings(['solutions_news', 'news', 'first_segment']);
\Route::group(['prefix' => \PublicPage::localePrefix().'/news', 'middleware' => 'public'], function() {

    Route::get('{news_url}', ['as' => 'public.news.show', function($news_url) {

        return \PublicNews::showNewsPage($news_url);
    }]);
});