<?php
namespace STALKER_CMS\Solutions\News\Http\Controllers;

use STALKER_CMS\Solutions\News\Models\News;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class NewsController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(News $news) {

        $this->model = $news;
        $this->middleware('auth');
    }

    public function index() {

        \PermissionsController::allowPermission('solutions_news', 'news');
        $request = \RequestController::init();
        $news = $this->model->whereLocale(\App::getLocale());
        if($request::has('sort_field') && $request::has('sort_direction')):
            foreach(explode(', ', $request::get('sort_field')) as $index):
                $news = $news->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if($request::has('search')):
            $search = $request::get('search');
            $news = $news->where(function($query) use ($search) {

                $query->where('title', 'like', '%'.$search.'%');
            });
        endif;
        $news = $news->orderBy('created_at', 'DESC')->paginate(10);
        return view('solutions_news_views::news.index', compact('news'));
    }

    public function create() {

        \PermissionsController::allowPermission('solutions_news', 'create');
        return view('solutions_news_views::news.create');
    }

    public function store() {

        \PermissionsController::allowPermission('solutions_news', 'create');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'));
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::makeOpenGraphData($request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($image = $this->uploadImages($request)):
                $request::merge($image);
            endif;
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.news.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function edit($id) {

        \PermissionsController::allowPermission('solutions_news', 'edit');
        $news = $this->model->findOrFail($id);
        return view('solutions_news_views::news.edit', compact('news'));
    }

    public function update($id) {

        \PermissionsController::allowPermission('solutions_news', 'edit');
        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $news = $this->model->findOrFail($id);
            if(\PermissionsController::isPackageEnabled('core_seo')):
                $this->model->forbiddenURL($request::input('seo_url'));
                $this->model->uniqueSeoURL($request::input('seo_url'), $id);
            endif;
            if(\PermissionsController::isPackageEnabled('core_open_graph')):
                $open_graph = \OpenGraph::remakeOpenGraphData($news, $request);
                $request::merge(['open_graph' => $open_graph]);
            endif;
            if($images = $this->uploadImages($request, $news)):
                $request::merge($images);
            else:
                $request::merge(['main_image' => $news->main_image, 'announce_image' => $news->announce_image]);
            endif;
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.news.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function destroy($id) {

        \PermissionsController::allowPermission('solutions_news', 'delete');
        \RequestController::isAJAX()->init();
        $news = $this->model->findOrFail($id);
        if(\PermissionsController::isPackageEnabled('core_open_graph')):
            \OpenGraph::destroyOpenGraphData($news);
        endif;
        $this->deleteImages($news);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.news.index'))->json();
    }

    /**************************************************************************************************************/
    private function uploadImages(\Request $request, $news = NULL) {

        if(!is_null($news)):
            $images = ['main_image' => $news->main_image, 'announce_image' => $news->announce_image];
        else:
            $images = ['main_image' => NULL, 'announce_image' => NULL];
        endif;
        if($request::input('main_image_delete') == 1):
            if(!empty($news->main_image) && \Storage::exists($news->main_image)):
                \Storage::delete($news->main_image);
                $news->main_image = NULL;
                $news->save();
            endif;
            $images['main_image'] = NULL;
        endif;
        if($request::input('announce_image_delete') == 1):
            if(!empty($news->announce_image) && \Storage::exists($news->announce_image)):
                \Storage::delete($news->announce_image);
                $news->announce_image = NULL;
                $news->save();
            endif;
            $images['announce_image'] = NULL;
        endif;
        if($request::hasFile('main_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('main_image')->getClientOriginalExtension();
            $mainPhotoPath = 'content/news';
            $request::file('main_image')->move('uploads/'.$mainPhotoPath, $fileName);
            $images['main_image'] = add_first_slash($mainPhotoPath.'/'.$fileName);
            if(!empty($news->main_image) && \Storage::exists($news->main_image)):
                \Storage::delete($news->main_image);
                $news->main_image = NULL;
                $news->save();
            endif;
        endif;
        if($request::hasFile('announce_image')):
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('announce_image')->getClientOriginalExtension();
            $announcePhotoPath = 'content/news/announce';
            $request::file('announce_image')->move('uploads/'.$announcePhotoPath, $fileName);
            $images['announce_image'] = add_first_slash($announcePhotoPath.'/'.$fileName);
            if(!empty($news->announce_image) && \Storage::exists($news->announce_image)):
                \Storage::delete($news->announce_image);
                $news->announce_image = NULL;
                $news->save();
            endif;
        endif;
        return $images;
    }

    private function deleteImages(News $news) {

        if(!empty($news->main_image) && \Storage::exists($news->main_image)):
            \Storage::delete($news->main_image);
            $news->main_image = NULL;
        endif;
        if(!empty($news->announce_image) && \Storage::exists($news->announce_image)):
            \Storage::delete($news->announce_image);
            $news->announce_image = NULL;
        endif;
        $news->save();
    }
}