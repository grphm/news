<?php
namespace STALKER_CMS\Solutions\News\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Solutions\News\Models\News;

class PublicNewsController extends ModuleController {

    protected $model;

    public function __construct() {

        $this->model = new News();
    }

    public function showNewsPage($url) {

        if(settings(['core_system', 'settings', 'services_mode'])):
            if(view()->exists("home_views::errors.1503")):
                return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            elseif(view()->exists("root_views::errors.1503")):
                return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
            endif;
        endif;
        if(is_numeric($url)):
            $page = $this->model->whereId($url)->wherePublication(TRUE)->with(['gallery.photos' => function($query) {

                $query->orderBy('order');
            }])->first();
        else:
            $page = $this->model->whereSeoUrl($url)->wherePublication(TRUE)->with(['gallery.photos' => function($query) {

                $query->orderBy('order');
            }])->first();
        endif;
        $view_path = $this->getViewPath($page->locale, 'news-single');
        if($page && view()->exists("home_views::$view_path")):
            $page->increment_views();
            return view("home_views::$view_path", compact('page'));
        else:
            abort(404);
        endif;
    }

    public function getTags($news_id = NULL) {

        $newsTags = [];
        $news = $this->model->whereLocale(\App::getLocale())
            ->wherePublication(TRUE)
            ->where('published_start', '<=', Carbon::now())
            ->where(function($query) {

                $query->where('published_stop', '>=', Carbon::now());
                $query->orWhere('published_stop', '');
                $query->orWhere('published_stop', NULL);
            });
        if(is_null($news_id)):
            foreach($news = $news->where('tags', '!=', '')->whereNotNull('tags')->lists('tags') as $news_tags):
                foreach(explode(',', $news_tags) as $tag):
                    if(!isset($newsTags[$tag])):
                        $newsTags[$tag] = 1;
                    else:
                        $newsTags[$tag]++;
                    endif;
                endforeach;
            endforeach;
        else:
            if($news = $news->whereId($news_id)->first()):
                foreach(explode(',', $news->tags) as $tag):
                    if(!isset($newsTags[$tag])):
                        $newsTags[$tag] = 1;
                    else:
                        $newsTags[$tag]++;
                    endif;
                endforeach;
            endif;
        endif;
        return $newsTags;
    }

    public function getNews($limit = NULL, $only_top = FALSE) {

        $news = $this->model->whereLocale(\App::getLocale())
            ->wherePublication(TRUE)
            ->where('published_start', '<=', Carbon::now())
            ->where(function($query) {

                $query->where('published_stop', '>=', Carbon::now());
                $query->orWhere('published_stop', '');
                $query->orWhere('published_stop', NULL);
            })
            ->orderBy('published_start', 'DESC')
            ->orderBy('created_at', 'DESC');
        if($only_top):
            $news = $news->whereTop(TRUE);
        endif;
        if(!is_null($limit)):
            return $news->paginate($limit);
        else:
            return $news->get();
        endif;
    }

    public function getTagNews($tag = NULL, $limit = NULL) {

        $news = $this->model->whereLocale(\App::getLocale())
            ->where('tags', 'like', '%'.$tag.'%')
            ->wherePublication(TRUE)
            ->where('published_start', '<=', Carbon::now())
            ->where(function($query) {

                $query->where('published_stop', '>=', Carbon::now());
                $query->orWhere('published_stop', '');
                $query->orWhere('published_stop', NULL);
            })
            ->orderBy('published_start', 'DESC')
            ->orderBy('created_at', 'DESC');
        if(!is_null($limit)):
            return $news->paginate($limit);
        else:
            return $news->get();
        endif;
    }

    private function getViewPath($locale , $template) {

        $locale_prefix = ($locale == settings(['core_system', 'settings', 'base_locale'])) ? '' : $locale.'.';
        return remove_first_slash($locale_prefix.$template);
    }
}