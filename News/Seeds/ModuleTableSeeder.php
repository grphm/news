<?php
namespace STALKER_CMS\Solutions\News\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'page', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Новости', 'en' => 'News', 'es' => 'Noticias']),
            'path' => 'news-collection.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        \DB::table('content_pages_templates')->insert([
            'menu_type' => 'page', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Новость', 'en' => 'News', 'es' => 'Noticias']),
            'path' => 'news-single.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
    }

    private function translate(array $trans) {

        return array_first($trans, function($key, $value) {

            return $key == \App::getLocale();
        });
    }
}