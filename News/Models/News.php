<?php
namespace STALKER_CMS\Solutions\News\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;
use STALKER_CMS\Core\OpenGraph\Traits\OpenGraphTrait;
use STALKER_CMS\Core\Seo\Traits\SeoTrait;

class News extends BaseModel implements ModelInterface {

    use ModelTrait, OpenGraphTrait, SeoTrait;
    protected $table = 'solution_news';
    protected $fillable = [
        'locale', 'publication', 'title', 'announce', 'content',
        'main_image', 'announce_image', 'gallery_id', 'tags', 'published_start', 'published_stop',
        'views', 'top', 'seo_title', 'seo_description', 'seo_keywords', 'seo_h1', 'seo_url',
        'open_graph', 'user_id', 'created_at', 'updated_at'
    ];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['published_start', 'published_stop', 'created_at', 'updated_at'];

    public function insert($request) {

        $this->locale = \App::getLocale();
        $this->publication = $request::has('publication') ? TRUE : FALSE;
        $this->title = $request::input('title');
        $this->announce = $request::input('announce');
        $this->content = $request::input('content');
        $this->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $this->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $this->gallery_id = $request::input('gallery_id');
        $this->tags = $request::input('tags');
        $this->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $this->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $this->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $this->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $this->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $this->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $this->published_start = $request::has('published_start') ? Carbon::parse($request::get('published_start'))->format('Y-m-d 00:00:00') : Carbon::now();
        $this->published_stop = $request::has('published_stop') ? Carbon::parse($request::get('published_stop'))->format('Y-m-d 23:59:59') : NULL;
        $this->views = 0;
        $this->top = $request::has('top') ? TRUE : FALSE;
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->publication = $request::has('publication') ? TRUE : FALSE;
        $model->title = $request::input('title');
        $model->announce = $request::input('announce');
        $model->content = $request::input('content');
        $model->main_image = $request::has('main_image') ? $request::input('main_image') : NULL;
        $model->announce_image = $request::has('announce_image') ? $request::input('announce_image') : NULL;
        $model->gallery_id = $request::input('gallery_id');
        $model->tags = $request::input('tags');
        $model->seo_title = $request::has('seo_title') && $request::input('seo_title') != '' ? $request::input('seo_title') : NULL;
        $model->seo_keywords = $request::has('seo_keywords') && $request::input('seo_keywords') != '' ? $request::input('seo_keywords') : NULL;
        $model->seo_description = $request::has('seo_description') && $request::input('seo_description') != '' ? $request::input('seo_description') : NULL;
        $model->seo_h1 = $request::has('seo_h1') && $request::input('seo_h1') != '' ? $request::input('seo_h1') : NULL;
        $model->seo_url = $request::has('seo_url') && $request::input('seo_url') != '' ? $request::input('seo_url') : NULL;
        $model->open_graph = $request::has('open_graph') ? $request::input('open_graph') : NULL;
        $model->published_start = $request::has('published_start') ? Carbon::parse($request::get('published_start'))->format('Y-m-d 00:00:00') : Carbon::now();
        $model->published_stop = $request::has('published_stop') ? Carbon::parse($request::get('published_stop'))->format('Y-m-d 23:59:59') : NULL;
        $model->top = $request::has('top') ? TRUE : FALSE;
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function getPublishedStartDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['published_start'])->diffForHumans();
    }

    public function getPublishedStopDateAttribute() {

        Carbon::setLocale(\App::getLocale());
        return Carbon::parse($this->attributes['published_stop'])->diffForHumans();
    }

    public function getInTopAttribute() {

        if($this->attributes['top']):
            return '<span class="c-green">'.\Lang::get('solutions_articles_lang::articles.in_top.yes').'</span >';
        else:
            return \Lang::get('solutions_articles_lang::articles.in_top.no');
        endif;
    }

    public function getAssetImageAttribute() {

        if($this->attributes['main_image']):
            return asset('uploads'.$this->attributes['main_image']);
        else:
            return NULL;
        endif;
    }

    public function getAssetAnnounceImageAttribute() {

        if($this->attributes['announce_image']):
            return asset('uploads'.$this->attributes['announce_image']);
        else:
            return NULL;
        endif;
    }

    public function getPublishedDateAttribute() {

        if($this->attributes['published_at']):
            Carbon::setLocale(\App::getLocale());
            return Carbon::parse($this->attributes['published_at'])->diffForHumans();
        else:
            return '<span class="c-red">'.\Lang::get('core_content_lang::pages.not_published').'</span >';
        endif;
    }

    public function getClassColorAttribute() {

        if($this->attributes['publication']):
            return 'green';
        else:
            return 'bluegray';
        endif;
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function gallery() {

        return $this->hasOne('\STALKER_CMS\Core\Galleries\Models\Gallery', 'id', 'gallery_id');
    }

    public function increment_views() {

        $this->views = $this->views + 1;
        $this->save();
        return $this;
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required'];
    }
}